class Question {
    constructor(question, reponses, goodAnswer) {
        this.question = question;
        this.reponses = reponses;
        this.goodAnswer = goodAnswer;
    }
    isCorrect(choice) {
        return choice === this.goodAnswer;
    }
}
export default Question