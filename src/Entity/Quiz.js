class Quiz {
    constructor(questions) {
        this.questions = questions;
        this.score = 0;
        this.questionIndex = 0;
    }
    getCurrentQuestion() {
        return this.questions[this.questionIndex];
    }
    isEnded(){       
        return this.questionIndex >= this.questions.length;
    }
    guess(answer){
        if (answer === this.getCurrentQuestion().goodAnswer){
            this.score ++;
        }
        this.questionIndex ++;
    }
}
export default Quiz