import './style.scss';
import Question from './Entity/Question';
import Quiz from './Entity/Quiz';

//creation du contenu des questions
const questions = [
    new Question('Quelle est la capitale du Vietnam ?', ['HochiMinhVille', 'Dalat', 'Hanoi', 'Sapa'], 'Hanoi'),
    new Question('De quelle couleur est le drapeau du Pakistan ?', ['Vert et blanc', 'Rouge et vert', 'Vert, jaune et noir', 'Rouge et vert'], 'Vert et blanc'),
    new Question('Graz est une ville ...', ['allemande', 'autrichienne', 'hongroise', 'slovaque'], 'autrichienne'),
    new Question('Quelle est la capitale du Rwanda ?', ['Nairobi', 'Porto-Novo', 'Kigali'],'Kigali'),
    new Question('Combien de pays ont une frontière terrestre avec la Colombie ?', ['2', '3', '4', '5', '6'], '5')
];

//creation du quiz et ajout des questions
let quiz = new Quiz(questions);

//fonction pour afficher le score à la fin du jeu
function showScores() {
    document.getElementById('score').innerText = quiz.score;
    document.getElementById('nbrQuestions').innerText = quiz.questions.length;
    document.getElementById('scorePopUp').classList.remove('hide');
};

//fonction pour lancer une nouvelle question
function populate() {
    if (quiz.isEnded()) {
        //si il n'y a plus de question, stopper le jeu et afficher le score
        showScores();        
    }
    else {
        //Mettre à jour la barre de progression (barre et %)
        let percentNbr = Math.floor(quiz.questionIndex / quiz.questions.length * 100)
        document.getElementById('gauge').style.width = `${percentNbr}%`;
        document.getElementById('pencentNbr').innerText = percentNbr;
        //Afficher la question dans le dom
        document.getElementById('question').innerText = quiz.getCurrentQuestion().question;
        //Vider les anciennes réponses
        document.getElementById('responses').innerHTML = ('');
        //Afficher les réponses dans les boutons
        for (let index = 0; index < quiz.getCurrentQuestion().reponses.length; index++) {
            //creation du template du bouton
            let tplBtn = document.createElement('button')
            tplBtn.textContent = `${quiz.getCurrentQuestion().reponses[index]}`;
            tplBtn.setAttribute('id', `answ${index}`);
            //ajout du bouton dans le dom
            document.getElementById('responses').appendChild(tplBtn);
            //Récupérer la réponse, vérifier si elle est bonne, incrémenter le score si ok, relancer une question
            document.getElementById(`answ${index}`).onclick = function () {
                quiz.guess(document.getElementById(`answ${index}`).innerText);
                populate();
            }
        }
    }
}
//Récupérer la réponse, vérifier si elle est bonne, incrémenter le score si ok, relancer une question
// for (let index = 0; index < quiz.getCurrentQuestion().reponses.length; index++) {
//     document.getElementById(`answ${index}`).onclick = function () {
//         quiz.guess(document.getElementById(`answ${index}`).innerText);
//         populate();
//     }
// };
//lancer la première question
populate();

//reload the page on restart click
document.getElementById('restart').addEventListener('click', function () {
    location.reload(true)
});